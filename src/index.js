import * as natsStreaming from 'node-nats-streaming';

import { setupDB } from './db';

const { BROKER_URL, BROKER_CLUSTER_ID } = process.env;

const stan = natsStreaming.connect(BROKER_CLUSTER_ID, 'user-sink', {
  url: BROKER_URL,
});

stan.on('connect', async () => {
  const db = await setupDB();

  const replayLastOpt = stan.subscriptionOptions().setStartWithLastReceived();

  const userSubs = stan.subscribe('user', replayLastOpt);
  userSubs.on('message', async (msg) => {
    const message = JSON.parse(msg.getData());
    const dbQueryResult = await db.collection('user').insertOne(message);

    const success =
      dbQueryResult.result.ok && dbQueryResult.insertedCount === 1;

    if (!success) {
      console.error('User was not created.');
    }
  });
});

stan.on('close', () => process.exit());
