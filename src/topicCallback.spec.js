import { topicCallback } from './topicCallback';

describe('topicCallback', () => {
  it('is defined', () => {
    expect(topicCallback).toBeDefined();
  });

  it('outputs the message', () => {
    const text = 'hello world',
      mockMsg = { getData: () => text };

    console.log = jest.fn();

    topicCallback(mockMsg);

    expect(console.log).toHaveBeenCalledWith(text);
  });
});
